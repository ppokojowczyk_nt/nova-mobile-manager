import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import { asyncComponent } from 'react-async-component';

import AppHeader from './AppHeader.js';
import AccountForm from './AccountForm.js';
import AccountsList from './AccountsList.js';

import './css/app.css';
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.compact.css';

/* globals __webpack_public_path__ */
__webpack_public_path__ = `${window.STATIC_URL}/assets/bundle/`;

class Myapp extends Component {
    render() {
        return (
            <Fragment>
                <div className="container">
                    <AppHeader/>
                </div>
                <div className="container">
                    <AccountsList/>
                </div>
            </Fragment>
        )
    }
}

render(<Myapp/>, document.getElementById('app'));
