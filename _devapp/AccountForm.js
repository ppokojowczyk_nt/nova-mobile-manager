import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';

import TextBox from 'devextreme-react/text-box';
import SelectBox from 'devextreme-react/text-box';
import dxForm from "devextreme/ui/form";
import Form from 'devextreme-react/form';
import DataSource from 'devextreme/data/data_source';

export default class AccountForm extends Component {

    ds = function(url){
        if (!url) { return false; }
        var store = new DataSource({
            load: function (loadOptions) {
                return fetch(url, {
                    method: 'POST',
                    credentials: 'include',
                    body: loadOptions
                }).then(r => {
					return r.json();
				});
            },
            byKey: function (key) {
                return {};
            }
        });
        return store;
    };

    formItems = [
        {dataField: "sim_id", caption: "simId"},
    ];

    constructor(props) {
        super(props);
        const Account = {};
        this.handleChange = this.handleChange.bind(this);
    }

    render () {
        return (
            <div>
                <div>
                    <Form
                        formData={this.state.Offer}
                        onFieldDataChanged={this.handleChange}
                        labelLocation='top'
                        items={this.formItems}
                    />
                </div>
                <div>
                    <pre>{JSON.stringify(this.state, undefined, 2)}</pre>
                </div>
            </div>
        )
    }

    handleChange(e) {
        this.setState({}); // __TODO__ how does this work??? devexpress handles this?
    }
}
