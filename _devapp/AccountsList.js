import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import DataGrid from 'devextreme-react/data-grid';
import DataSource from 'devextreme/data/data_source';
import CustomStore from 'devextreme/data/custom_store';

const instancesDataSource = {
    store: new CustomStore({
        load: function (loadOptions) {
            // let params = '?';
            // params += `skip=${loadOptions.skip}`;
            // params += `&take=${loadOptions.take}`;
            // if (loadOptions.sort) {
            //     params += `&orderby=${loadOptions.sort[0].selector}`;
            //     if (loadOptions.sort[0].desc) {
            //         params += ' desc';
            //     }
            // }
            return fetch(__API_URL__ + '?action=instances', {
                    mode: "cors",
                    cache: 'no-cache'
                })
                .then(response => response.json())
                .then((data) => {
                    return {
                        data: data,
                        totalCount: data.length
                    };
                })
                .catch(() => { throw 'Data Loading Error'; });
        }
    })
};

const usersDataSources = {
    store: new CustomStore({
        load: function (loadOptions) {
            // let params = '?';
            // params += `skip=${loadOptions.skip}`;
            // params += `&take=${loadOptions.take}`;
            // if (loadOptions.sort) {
            //     params += `&orderby=${loadOptions.sort[0].selector}`;
            //     if (loadOptions.sort[0].desc) {
            //         params += ' desc';
            //     }
            // }
            return fetch(__API_URL__ + '?action=users', {
                    mode: "cors",
                    cache: 'no-cache'
                })
                .then(response => response.json())
                .then((data) => {
                    return {
                        data: data,
                        totalCount: data.length
                    };
                })
                .catch(() => { throw 'Data Loading Error'; });
        }
    })
};

export default class AccountsList extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        
        const columns = [
            { caption: "Login", dataField: "login" },
            { caption: "SimID", dataField: "sim_id" },
            { caption: "Instance", dataField: "instance_id", lookup: {
                dataSource: this.fx,
                valueExpr: "id",
                displayExpr: "name"
            }},
        ];

        const styling = {height: "50%"};

        return (
            <DataGrid style={styling}
                searchPanel={{enabled: true}}
                dataSource={usersDataSources}
                columns={columns}
                editing={{
                    enabled: true,
                    mode: "form",
                    allowAdding: true,
                    allowUpdating: true,
                    allowDeleting: true
                }}
                onInitialized={this.onGridInitialized}
            />
        )
    }

    onGridInitialized(e){
        console.log(e);
        instancesDataSource.store.load().then(data => {
            var fx = data.data;
            e.component.columnOption("instance_id", "lookup", {
                dataSource: fx,
                valueExpr: "id",
                displayExpr: "name"
            });
        });
    }

    handleChange(e) {
        this.setState({}); // __TODO__ how does this work??? devexpress handles this?
    }

    componentDidMount(){
    }
}
