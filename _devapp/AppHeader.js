import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import logo from './images/logo.png';
import myApp from 'myApp';

class AppHeader extends Component {

    render() {
        const { user: { name, email }, logged } = myApp;
        return (
            <div className="app-header">
                <div className="app-logo">{__APP_NAME__}</div>
                <div className="f-right session">
                    {logged && <p>Logged in as <span className='color--hl'>{name} [ {email} ] </span></p>}
                    <p>API URL <span className="color2">{__API_URL__}</span></p>
                </div>
            </div>
        );
    }

}

export default AppHeader;
