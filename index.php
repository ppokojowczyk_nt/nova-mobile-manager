<?php
$user = (object) [
    'name' => 'Piotr Pokojowczyk',
    'email' => 'p.pokojowczyk@nova-tracking.com',
    'logged' => true
];
?>
    <script type="text/javascript">
        var STATIC_URL = 'http://localhost/nova-mobile-manager/';
        var myApp = {
            user: <?php echo json_encode($user); ?>,
            logged: <?php echo $user->logged; ?>
        };
    </script>
    <div id="app"></div>
    <script type="text/javascript" src="assets/bundle/main.bundle.js" ></script>
    