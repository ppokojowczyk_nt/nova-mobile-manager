'use strict';

const ASSET_PATH = process.env.ASSET_PATH || '/';

const webpack = require('webpack');
        const path = require('path');
        const ExtractTextPlugin = require("extract-text-webpack-plugin");
        let config = {
        entry: {
        // __TODO__ JS sources and styles
        main: [
                './_devapp/app.js'
                // './_devapp/css/app.css'
        ]
        },
                output: {
                    // __TODO__ output compiled scripts here
                    path: path.resolve(__dirname, 'assets', 'bundle'),
                    filename: '[name].bundle.js',
                    //publicPath: ASSET_PATH
                    publicPath: 'assets' // __TODO__ how to better configure this ?
                }
        ,
                resolve: {
                    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx']
                }
        ,
                module: {
                    rules: [
                        {
                            test: /\.(js|jsx|tsx|ts)$/,
                            exclude: path.resolve(__dirname, 'node_modules'),
                            use: {
                                loader: 'babel-loader',
                                options: {
                                    presets: [
                                        '@babel/preset-env',
                                        '@babel/preset-react',
                                        '@babel/preset-typescript'
                                    ],
                                    plugins: [
                                        ["@babel/plugin-proposal-decorators", {"legacy": true}],
                                        '@babel/plugin-syntax-dynamic-import',
                                        ['@babel/plugin-proposal-class-properties', {"loose": true}]
                                    ]
                                }
                            },
                        },
                        {
                            test: /\.scss$/,
                            use: ExtractTextPlugin.extract({
                                fallback: 'style-loader',
                                use: [
                                    {
                                        loader: 'css-loader',
                                    },
                                    // __TODO__ do i need this
//                                    'postcss-loader',
//                                    'sass-loader'
                                ]
                            })
                        },
                        {
                            // test: /.(png|woff(2)?|eot|ttf|svg|gif)(\?[a-z0-9=\.]+)?$/,
                            test: /.(png|svg|gif)(\?[a-z0-9=\.]+)?$/,
                            use: [
                                {
                                    loader: 'file-loader',
                                    options: {
                                        //name: '../css/[hash].[ext]' // __TODO__ this ? or the one below ?
                                        name: '/css/[hash].[ext]'
                                    }
                                }
                            ]
                        },
                        {
                            test: /\.css$/,
                            use: [
                                'style-loader',
                                'css-loader'
                                // __TODO__ do i need this
                                //'postcss-loader'
                            ]
                        },
                        // { 
                        //     test: /\.(eot|svg|ttf|woff|woff2)$/,
                        //     use: "url-loader?name=[name].[ext]"
                        // }
                        //{ test: /\.css$/, loader: "style-loader!css-loader" },
                       {
                           test: /\.(ttf|eot|woff(2)?)$/,
                           use: [
                               "file-loader?name=/[name].[ext]"
                           ]
                       }
                    ]
                }
        ,
                externals: {
                myApp: 'myApp',
                },
                plugins: [
                        new ExtractTextPlugin(path.join('..', 'css', 'app.css')), // __TODO__ mix assets or not?
                        new webpack.DefinePlugin({
                            'process.env.ASSET_PATH': JSON.stringify(ASSET_PATH),
                            '__DEV__' : JSON.stringify(true),
                            '__API_URL__' : JSON.stringify('http://mobile-api.nova-tracking.com/'),
                            '__APP_NAME__' : JSON.stringify('Mobile API Manager')
                        }),
                ],
        };
if (process.env.NODE_ENV === 'production') {
    config.plugins.push(
            new webpack.optimize.UglifyJsPlugin({
                sourceMap: false,
                compress: {
                    sequences: true,
                    conditionals: true,
                    booleans: true,
                    if_return: true,
                    join_vars: true,
                    drop_console: true
                },
                output: {
                    comments: false
                },
                minimize: true
            })
            );
}

module.exports = config;
